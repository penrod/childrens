trigger ClassroomTrigger on Classroom__c (before insert, after insert) {
    if (trigger.isAfter && trigger.isInsert) {
        ClassroomTriggerHelper.createStudents(0,trigger.new);
    }
    else if (trigger.isBefore && trigger.isInsert) {
        ClassroomTriggerHelper.setSchool(trigger.new);
    }
}