trigger RegistrationTrigger on Registration__c (after insert, after update, before insert) {
    if (trigger.isInsert && trigger.isafter)
        RegistrationTriggerHelper.addEnrollments(trigger.new);
    else if (trigger.isInsert && trigger.isBefore) 
        RegistrationTriggerHelper.duplicateCheck(trigger.new);    	
    else if (trigger.isUpdate)
        RegistrationTriggerHelper.addUGA(trigger.newMap, Trigger.oldMap);
}