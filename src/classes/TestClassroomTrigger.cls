@isTest
private class TestClassroomTrigger {
    static testMethod void testClassroom () {
        Contact cont = new Contact(FirstName='Test', LastName='Contact', Email='test@test.com');
        insert cont;
        Test.startTest();
        Classroom__c cRoom = new Classroom__c(Name='Math', Primary_Contact__c=cont.ID, Student_Logins_Required__c=3, Individual_Student_Logins_Needed__c=true);
        insert cRoom;
        Test.stopTest();
        List<Contact> lStudents = [SELECT ID FROM Contact WHERE Classroom__c=:cRoom.ID];
        System.assertEquals(3,lStudents.size());
    }
    static testMethod void testAddStudents () {
        Contact cont = new Contact(FirstName='Test', LastName='Contact', Email='test@test.com');
        insert cont;
        Classroom__c cRoom = new Classroom__c(Name='Math', Primary_Contact__c=cont.ID, Student_Logins_Required__c=3, Individual_Student_Logins_Needed__c=true);
        insert cRoom;
        cRoom.Additional_Logins_Needed__c=3;
        update cRoom;
        Test.startTest();
        ClassroomTriggerHelper.createMoreStudents(cRoom.ID);
        Test.stopTest();
        Integer newsize = [SELECT ID FROM Contact WHERE Classroom__c=:cRoom.ID].size();
        System.assertequals(6,newsize);
    }
}