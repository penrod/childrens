@isTest (seeAllData=true)
public class TestRegistrationTrigger {
    static testMethod void indReg () {
        Contact cont = new Contact(FirstName='Test', LastName='Contact');
        insert cont;
        enlighten__Course__c course =[SELECT ID FROM enlighten__Course__c WHERE RecordType.Name='Individual' And Access_Code__c=null LIMIT 1];
    	Test.startTest();
        Registration__c reg = new Registration__c(Course__c=course.ID, Contact__c=cont.ID, I_accept_the_terms_and_conditions__c=true);
        insert reg;
        Test.stopTest();
        List<enlighten__Enrollment__c> lEnroll=[SELECT ID FROM enlighten__Enrollment__c WHERE Registration__c=:reg.ID];
        System.assertEquals(1,lEnroll.size());
    }
    static testMethod void k8Reg(){
        Contact cont = new Contact(FirstName='Test', LastName='Contact');
        insert cont;
        enlighten__Course__c course =[SELECT ID FROM enlighten__Course__c WHERE RecordType.Name='K-8' And Access_Code__c=null LIMIT 1];
    	Classroom__c cRoom = new Classroom__c(Name='Math', Primary_Contact__c=cont.ID, Student_Logins_Required__c=2, Individual_Student_Logins_Needed__c=true);
        insert cRoom;
        Test.startTest();
        Registration__c reg = new Registration__c(Course__c=course.ID, Contact__c=cont.ID, Classroom__c=cRoom.ID, I_accept_the_terms_and_conditions__c=true);
        insert reg;
        Test.stopTest();
        List<enlighten__Enrollment__c> lEnroll=[SELECT ID FROM enlighten__Enrollment__c WHERE Registration__c=:reg.ID];
        System.assertEquals(3,lEnroll.size());
    }
    static testMethod void testUGA() {
        Contact cont = new Contact(FirstName='Test', LastName='Contact');
        insert cont;
        enlighten__Course__c course =[SELECT ID FROM enlighten__Course__c WHERE RecordType.Name='K-8' And Access_Code__c=null LIMIT 1];
    	Classroom__c cRoom = new Classroom__c(Name='Math', Primary_Contact__c=cont.ID, Student_Logins_Required__c=2, Individual_Student_Logins_Needed__c=true);
        insert cRoom;
        Registration__c reg = new Registration__c(Course__c=course.ID, Contact__c=cont.ID, Classroom__c=cRoom.ID, I_accept_the_terms_and_conditions__c=true);
        insert reg;
        test.startTest();
        reg.Create_UGA__c=true;
        update reg;
        test.stopTest();
        Integer numUGA = [SELECT ID FROM enlighten__Course_User_Group_Assignment__c WHERE enlighten__Group__c in (SELECT ID FROM enlighten__Course_User_Group__c WHERE Registration__c=:reg.ID)].size();
    	System.assertEquals(3,numUGA);
    }
}