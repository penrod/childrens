public without sharing class RegisterController {
    
    public String memberType { get; set; }

    public String courseType { get; set; }
    public String firstName { get; set; }
    public String lastName { get; set; }
    public String email { get; set; }
    public String confirmEmail { get; set; }
    public String phone { get; set; }
    public String schoolDistrict { get; set; }
    public String school { get; set; }
    public String city { get; set; }
    public String county { get; set; }
    public String state { get; set; }
    public String zip { get; set; }
    public String jobTitle { get; set; }
    public String organization { get; set; }
    public String organizationValue {get; set;}
    public List<SelectOption> courseTypes {get; set;}
    public List<String> listOfOrgs { get; set; }
    public List<String> schoolDistricts {get; set;}
    public List<SelectOption> schools {get; set;}
    public boolean showSchoolDistricts {get; set;}
    public boolean showSchools {get; set;}
    public boolean showOrganization {get; set;}
    public boolean showJobTitle {get; set;}
    public string organizationStyle {get; set;}
    public string searchTerm{get; set;}
<<<<<<< HEAD
=======
    public string inquiryLink {get; set;}
>>>>>>> c52b006bf2e489d25cd63182dfd52eebbc36f7f1

    public RegisterController() {
        SetCourseTypes();
        SetSchoolDistricts();
        SetSchoolsForDistrict();
<<<<<<< HEAD
        SetVisibilityBooleans();      
=======
        SetVisibilityBooleans();

        inquiryLink = 'http://healthykidslearnmore.com/contactus.asp';        
>>>>>>> c52b006bf2e489d25cd63182dfd52eebbc36f7f1
    }

    public void SetCourseTypes(){
        courseTypes = new List<SelectOption>();
        courseTypes.add(new SelectOption('', 'Select a course Type'));
        courseTypes.add(new SelectOption('Educator', 'I am looking to register for a K-8 course for one school within my district'));
        courseTypes.add(new SelectOption('EducatorMultiple', 'I am looking to register for a K-8 course for multiple schools within my district'));
        courseTypes.add(new SelectOption('Organizational', 'I am looking for a professional development course for work'));
        courseTypes.add(new SelectOption('Individual', 'I am looking for an education course for myself or my child'));
    }

    
    public void SetVisibilityBooleans(){
        System.debug('Course Type: ' + courseType);
        if (String.isBlank(courseType) || courseType == 'Educator'){
            showSchoolDistricts = true;
            showSchools = true;
            showOrganization = false;
            showJobTitle = true;
            organizationStyle = 'display:none';
        } else if (courseType == 'EducatorMultiple'){
            showSchoolDistricts = true;
            showSchools = false;
            showOrganization = false;
            showJobTitle = true;
            organizationStyle = 'display:none';
        }  else if (courseType == 'Organizational'){
            showSchoolDistricts = false;
            showSchools = false;
            showOrganization = true;
            showJobTitle = true;
            organizationStyle = 'display:block';
        } else if (courseType == 'Individual'){
            showSchoolDistricts = false;
            showSchools = false;
            showOrganization = false;
            showJobTitle = false;
            organizationStyle = 'display:none';

        }
        System.debug('showSchoolDistricts: ' + showSchoolDistricts);
        System.debug('showSchools: ' + showSchools);
        System.debug('showOrganization: ' + showOrganization);
        System.debug('showJobTitle: ' + showJobTitle);
    }
    
    public PageReference registerUser() {
        System.debug('Register User');
        try {
            System.debug('ORGANIZATION: ' + organization);
            System.debug('ORGANIZATION VALUE: ' + organizationValue);
            Contact contact;
            
            String userName = email;
            Organization org = [SELECT Id, InstanceName,  IsSandbox FROM Organization LIMIT 1];
            if(org.IsSandbox){
                userName += '.' + org.InstanceName;
            }

            // Validate Member input, return null to display error message
            if ( !validateUserInput() ) return Page.ch_register;

            // Check if user exists
            if (UserExists(userName)) {
                ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR,'A user associated with that email already exists');
                ApexPages.addMessage(error);
                System.debug(error);
                return Page.ch_register;
            }

            Id profileId = getProfileIdForUser();

            Account account = getAccountForContact();

            contact = createContact(firstName, lastName, email, phone, city, county, state, zip, account.Id);
            string companyName;
            if (courseType == 'Educator' || courseType == 'Organizational' || courseType == 'EducatorMultiple'){
                companyName = account.Name;
            }

            createUser(firstName, lastName, email, userName, jobTitle, phone, companyName, contact.Id, profileId);
            return Page.ch_register_success;

        } catch(DmlException e) {
            System.debug(e.getMessage());
            if (e.getMessage().contains('DUPLICATE_USERNAME')){
                ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR,'A user associated with this email already exists.'); //user probably already exists
                ApexPages.addMessage(error);
            }else if (e.getMessage().contains('FIELD_INTEGRITY_EXCEPTION')){
                ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR,'A user associated with this email already exists.'); //user probably already exists
                ApexPages.addMessage(error);
            }else {                
                ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR,'An unexpected error occurred, please try again.'); //user probably already exists
                ApexPages.addMessage(error);
            }
            System.debug('Messages: ' + apexPages.getMessages());
            return Page.ch_register;
        } catch(Exception e) {
            System.debug(e.getMessage());
            ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR,'An unexpected error occurred, please try again.'); //user probably already exists
            ApexPages.addMessage(error);
            return Page.ch_register;
        }

        ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR,'An unexpected error occurred, please try again.'); //user probably already exists
        ApexPages.addMessage(error);
        return null;

    }

    @TestVisible
    private Boolean validateUserInput() {
        if ( String.isBlank(courseType) ) {
            ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR, 'Select a course type.');
            ApexPages.addMessage(error);
            return false;
        }

        if ( String.isBlank(firstName) || String.isBlank(lastName) ) {
            ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR, 'First and last name are required.');
            ApexPages.addMessage(error);
            return false;
        }

        if ( String.isBlank(email) ) {
            ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR, 'Email is required.');
            ApexPages.addMessage(error);
            return false;
        }

        if ( String.isBlank(phone) ) {
            ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR, 'Phone number is required.');
            ApexPages.addMessage(error);
            return false;
        }

        if ( String.isBlank(city) ) {
            ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR, 'City is required.');
            ApexPages.addMessage(error);
            return false;
        }

        if ( String.isBlank(county) ) {
            ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR, 'County is required.');
            ApexPages.addMessage(error);
            return false;
        }

        if ( String.isBlank(state) ) {
            ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR, 'State is required.');
            ApexPages.addMessage(error);
            return false;
        }

        if ( String.isBlank(zip) ) {
            ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR, 'Zip is required.');
            ApexPages.addMessage(error);
            return false;
        }

        if (courseType == 'Educator'){
            if ( String.isBlank(schoolDistrict) ) {
                ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR, 'School District is required.');
                ApexPages.addMessage(error);
                return false;
            }

            if ( String.isBlank(school) ) {
                ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR, 'School is required.');
                ApexPages.addMessage(error);
                return false;
            }

            if ( String.isBlank(jobTitle) ) {
                ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR, 'Job Title is required.');
                ApexPages.addMessage(error);
                return false;
            }
        }else if (courseType == 'EducatorMultiple'){
            if ( String.isBlank(schoolDistrict) ) {
                ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR, 'School District is required.');
                ApexPages.addMessage(error);
                return false;
            }

            if ( String.isBlank(jobTitle) ) {
                ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR, 'Job Title is required.');
                ApexPages.addMessage(error);
                return false;
            }
        }else if (courseType == 'Organizational'){
            if ( String.isBlank(organizationValue) ) {
                ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR, 'Organization is required.');
                ApexPages.addMessage(error);
                return false;
            }

            if ( String.isBlank(jobTitle) ) {
                ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR, 'Job Title is required.');
                ApexPages.addMessage(error);
                return false;
            }
        }else if (courseType == 'Individual'){
        
        }
        return true;
    }

    public static Boolean userExists(String email) {

        List<User> users = [
            SELECT Id
            FROM User
            WHERE username = :email
        ];

        return users.size() > 0;
    }
    
<<<<<<< HEAD
    public Id getProfileIdForUser(){
=======
    private Id getProfileIdForUser(){
>>>>>>> c52b006bf2e489d25cd63182dfd52eebbc36f7f1

        String userProfile = 'Customer Community Plus ';
        if (courseType == 'Educator'){
            userProfile += 'K-8';
        }else if (courseType == 'EducatorMultiple'){
            userProfile += 'K-8 Multiple';
        }else if (courseType == 'Organizational'){
            userProfile += 'Individual';
        }else if (courseType == 'Individual'){
            userProfile += 'Individual';
        }

        System.debug('Profile  Name: ' + userProfile);
        Id profileId = [
            SELECT Id, Name
            FROM Profile
            WHERE Name = :userProfile
        ].Id;

        return profileId;
    }
    
    public static void createUser(string firstName, string lastName, string email, string username, string jobTitle, string phoneNumber, string companyName, string contactId, string profileId) {
        
        User user = new User();
        user.FirstName = firstName;
        user.LastName = lastName;
        user.Email = email;
        user.Username = username;
        user.alias = 'commu';
        user.languagelocalekey = 'en_US';
        user.localesidkey = 'en_US';
        System.debug('Profile: ' + profileId);
        user.profileId = profileId;
        user.contactId = contactId;
        user.emailencodingkey = 'UTF-8';
        user.CommunityNickname = email.split('@')[0];
<<<<<<< HEAD
        Integer nicknameNum = randomWithLimit(1000);
        user.CommunityNickname += String.valueOf(nicknameNum);
=======
>>>>>>> c52b006bf2e489d25cd63182dfd52eebbc36f7f1
        user.timezonesidkey = 'America/New_York';
        user.Title = jobTitle;
        user.CompanyName = companyName;
        user.Phone = phoneNumber;
        try {
            insert user;
        }
        catch (DmlException ex) {
            System.debug('DML Error: ' + ex.getMessage());
            throw ex;
        }
    }

    public void SetSchoolDistricts(){
        schoolDistricts = new List<String>();

        for (Account a : [SELECT Id, Name FROM Account WHERE RecordType.DeveloperName = 'School_District']){
            schoolDistricts.add(a.Name);
        }

        if (courseType == 'Educator'){
            showSchools = true;
        } else {
            showSchools = false;
        }
        /*
        schoolDistricts.add(new SelectOption('0', 'Select a School District'));
        for (Account a : accounts){
            schoolDistricts.add(new SelectOption(a.Id, a.Name));
        }*/
    }

    public Contact CreateContact(String firstName, String lastName, String email, String phone, String city, String county, String state, String zip, String accountId){
        string recordTypeId;
        if (courseType == 'Educator' || courseType == 'EducatorMultiple'){
            recordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Contact' AND DeveloperName = 'K_8'].Id;
        }else if (courseType == 'Organizational'){
            recordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Contact' AND DeveloperName = 'Individual'].Id;
        }else if (courseType == 'Individual'){
            recordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Contact' AND DeveloperName = 'Individual'].Id;
        }
        
        Contact c = new Contact();
        c.FirstName = firstName;
        c.LastName = lastName;
        c.Email = email;
        c.Phone = phone;
        c.MailingCity = city;
        c.MailingState = state;
        c.MailingPostalCode = zip;
        c.RecordTypeId = recordTypeId;
        c.AccountId = accountId;

        insert c;
        return c;
    }

    public void SetSchoolsForDistrict(){
        System.debug('SETTING SCHOOLS FOR: ' + schoolDistrict);
        schools = new List<SelectOption>();
        schools.add(new SelectOption('', 'Select a School'));

        if (String.isNotBlank(schoolDistrict)){
            List<Account> accounts = [SELECT Id, Name FROM Account WHERE RecordType.DeveloperName = 'School' AND Parent.Name = :schoolDistrict ORDER BY Name];
            for (Account a : accounts){
                schools.add(new SelectOption(a.Id, a.Name));
            }
        }
    }

    public Account GetAccountForContact(){
        Account account;
        string recordTypeId;
        System.debug('Organization: ' + organizationValue);

        if (courseType == 'Educator'){
            //Get the school account
            account = [SELECT Id, Name FROM Account WHERE Id = :school LIMIT 1];
        }else if (courseType == 'EducatorMultiple'){
            //Get the school district account
            account = [SELECT Id, Name FROM Account WHERE Name = :schoolDistrict LIMIT 1];
        }else if (courseType == 'Organizational'){
            //Get the org account
            List<Account> accounts = [SELECT Id, Name FROM Account WHERE Name = :organizationValue LIMIT 1];
            if (accounts.size() < 1){
                //Create new Org account
                RecordType rt = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND DeveloperName = 'Organization' LIMIT 1];
                account = new Account();
                account.Name = organizationValue;
                account.RecordTypeId = rt.Id;
                Registration_Setting__mdt registrationSetting = [SELECT Id, Owner_Id__c FROM Registration_Setting__mdt WHERE DeveloperName = 'Organization_Account_Owner' LIMIT 1];
                account.OwnerId = registrationSetting.Owner_Id__c;
                insert account;
            }else {
                account = accounts[0];
            }
        }else if (courseType == 'Individual'){
            //Create a new account
            RecordType rt = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND DeveloperName = 'Household' LIMIT 1];
            account = new Account();
            account.Name = firstName + ' ' + lastName;
            account.RecordTypeId = rt.Id;
            Registration_Setting__mdt registrationSetting = [SELECT Id, Owner_Id__c FROM Registration_Setting__mdt WHERE DeveloperName = 'Individual_Account_Owner' LIMIT 1];
            account.OwnerId = registrationSetting.Owner_Id__c;
            insert account;
        }

        return account;
    }
    
    // JS Remoting action called when searching for a movie name
    @RemoteAction
    public static List<Account> searchOrganization(String searchTerm) {
        System.debug('Organization Name is: '+searchTerm );
        List<Account> organizations = Database.query('Select Id, Name from Account where name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\' AND RecordType.DeveloperName = \'Organization\'');
        return organizations ;
<<<<<<< HEAD
    }  

    public static Integer randomWithLimit(Integer upperLimit){
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, upperLimit);
    }  
=======
    }    
>>>>>>> c52b006bf2e489d25cd63182dfd52eebbc36f7f1
}