global class ClassroomTriggerHelper {
    public static void createStudents(integer startnum, List<Classroom__c> lClassroom) {
        //List<Classroom__c> lClassroom = [SELECT ID, Student_Logins_Required__c, Primary_Contact__c FROM Classroom__c WHERE ID in :trigger.new];
    	ID studentRT = [SELECT ID FROM RecordType WHERE sObjectType='Contact' AND Name='Student' LIMIT 1].ID;
        List<Contact> contToIns = new List<Contact>();
        for (Classroom__c c : lClassroom) {
       	 	// get number of students
       	 	system.debug('individual logins needed? '+c.Individual_Student_Logins_Needed__c);
       	 	if(c.Individual_Student_Logins_Needed__c==true) {
            	Contact primC= [SELECT ID, AccountID, FirstName, LastName FROM Contact WHERE ID=:c.Primary_Contact__c];
            	decimal numstudents;
                if (c.Additional_Logins_Needed__c==0 || c.Additional_Logins_Needed__c==null)
                	numstudents=c.Student_Logins_Required__c;
                else
                    numstudents=c.Additional_Logins_Needed__c;
        		system.debug('acctID: '+primC.AccountID);
        		system.debug('classroom id: '+c.ID);
        		system.debug('student logins required: '+numstudents);
                system.debug('additional logins needed: '+c.Additional_Logins_Needed__c);
                system.debug('student logins needed: '+c.Student_Logins_Required__c);
        		// iterate and create as many students as in Student Logins Required field
        		// list of password words
        		List<string> pList = new List<string> {'learn','teach','school','class','create','active','healthy','lesson','know'};
        		for (integer i =0; i<numstudents; i++) {
         			String stemailsp='st'+string.valueof(startnum)+'@'+primC.FirstName.left(3)+primC.LastName.left(4)+'.com';
                	String stemail = stemailsp.deleteWhiteSpace();
         	   		Integer rand1 = Math.round(Math.random()*8);
              		Integer rand2 = Math.round(Math.random()*9);
              		Integer rand3 = Math.round(Math.random()*9);
         	   		String pw = pList[rand1]+rand2+rand3;
          	  		Contact newstudent = new Contact(FirstName='Student', LastName=string.valueof(startnum+1), Email=stemail, enlighten__MoodleUsername__c=stemail, Moodle_Password__c=pw, RecordTypeID=studentRT, Classroom__c=c.ID, AccountID=c.School__c, Static_Password__c=true, OwnerID=userInfo.getUserId());
          	  		System.debug('User ID: '+newStudent.OwnerID);
          	  		ContToIns.add(newstudent);
                    startnum++;
         		}
        	}
    	}
    	try {
    	    if (!contToIns.isEmpty())
     	       insert contToIns;
    	}
    	catch (system.DmlException e) {
        
    	}
    } 
    webservice static void createMoreStudents (ID cID) {
<<<<<<< HEAD
        Classroom__c c = [SELECT ID, Additional_Logins_Needed__c, Student_Logins_Required__c, Primary_Contact__c, Individual_Student_Logins_Needed__c, School__c FROM Classroom__c WHERE ID=:cID LIMIT 1];
=======
        Classroom__c c = [SELECT ID, Additional_Logins_Needed__c, Student_Logins_Required__c, Primary_Contact__c, Individual_Student_Logins_Needed__c FROM Classroom__c WHERE ID=:cID LIMIT 1];
>>>>>>> c52b006bf2e489d25cd63182dfd52eebbc36f7f1
        List<Classroom__c> lC = new List<Classroom__c> {c};
        integer startnum = [SELECT ID FROM Contact WHERE Classroom__c=:+cID].size();
        ClassroomTriggerHelper.createStudents(startnum+1, lC);
        ClassroomTriggerHelper.updateNumStudents(c);
    }
    public static void updateNumStudents (Classroom__c c) {
        c.Student_Logins_Required__c += c.Additional_Logins_Needed__c;
        c.Additional_Logins_Needed__c = 0;
        update c;
    }
    public static void setSchool (List<Classroom__c> lC) {
        for (Classroom__c c : lC) {
            if (c.School__c==null) {
                c.School__c=[SELECT AccountID FROM Contact WHERE ID=:c.Primary_Contact__c LIMIT 1].AccountID;
            }
        }
    }
}