public class k8RegConfController {
    public ID registrationID {get;set;}
    public List<Contact> getStudents() {
        // get enrollments for the provided registration
        List<enlighten__Enrollment__c> lEnrollments = [SELECT ID, enlighten__Contact__c FROM enlighten__Enrollment__c WHERE Registration__c=:registrationID];
        // get students from enrollments
        List<ID> lContIDs = new List<ID> ();
        for (enlighten__Enrollment__c e : lEnrollments) {
            lContIds.add(e.enlighten__Contact__c);
        }
        List<Contact> lContacts = [SELECT Id, enlighten__MoodleUsername__c, Moodle_Password__c, FirstName, LastName FROM Contact WHERE ID in: lContIds];
        return lContacts;
    }
}