@isTest
public with sharing class TestRegisterController {

	@isTest
	public static void testSetCourseTypes(){
		RegisterController registerController = new RegisterController();
		registerController.SetCourseTypes();

		System.assertEquals(5, registerController.courseTypes.size());
	}

    @isTest
    public static void testSetVisibilityBooleanEducator(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Educator';
		registerController.SetVisibilityBooleans();

		System.assertEquals(true, registerController.showSchoolDistricts);
		System.assertEquals(true, registerController.showSchools);
		System.assertEquals(false, registerController.showOrganization);
		System.assertEquals(true, registerController.showJobTitle);
		System.assertEquals('display:none', registerController.organizationStyle);
    }

    @isTest
    public static void testSetVisibilityBooleanEducatorMultiple(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'EducatorMultiple';
		registerController.SetVisibilityBooleans();

		System.assertEquals(true, registerController.showSchoolDistricts);
		System.assertEquals(false, registerController.showSchools);
		System.assertEquals(false, registerController.showOrganization);
		System.assertEquals(true, registerController.showJobTitle);
		System.assertEquals('display:none', registerController.organizationStyle);
    }

    @isTest
    public static void testSetVisibilityBooleanOrganizational(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Organizational';
		registerController.SetVisibilityBooleans();

		System.assertEquals(false, registerController.showSchoolDistricts);
		System.assertEquals(false, registerController.showSchools);
		System.assertEquals(true, registerController.showOrganization);
		System.assertEquals(true, registerController.showJobTitle);
		System.assertEquals('display:block', registerController.organizationStyle);
    }

    @isTest
    public static void testSetVisibilityBooleanIndividual(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Individual';
		registerController.SetVisibilityBooleans();

		System.assertEquals(false, registerController.showSchoolDistricts);
		System.assertEquals(false, registerController.showSchools);
		System.assertEquals(false, registerController.showOrganization);
		System.assertEquals(false, registerController.showJobTitle);
		System.assertEquals('display:none', registerController.organizationStyle);
    }

    @isTest
    public static void testValidateUserInputNoCourse(){
		RegisterController registerController = new RegisterController();
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(false, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(1, messages.size());
		system.assertEquals('Select a course type.', messages[0].getSummary());
    }

    @isTest
    public static void testValidateUserInputNoName(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Educator';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(false, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(1, messages.size());
		system.assertEquals('First and last name are required.', messages[0].getSummary());
    }

    @isTest
    public static void testValidateUserInputNoEmail(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Educator';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(false, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(1, messages.size());
		system.assertEquals('Email is required.', messages[0].getSummary());
    }

    @isTest
    public static void testValidateUserInputNoPhoneNumber(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Educator';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'email';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(false, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(1, messages.size());
		system.assertEquals('Phone number is required.', messages[0].getSummary());
    }

    @isTest
    public static void testValidateUserInputNoCity(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Educator';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'email';
		registerController.phone = '444-444-4444';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(false, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(1, messages.size());
		system.assertEquals('City is required.', messages[0].getSummary());
    }

    @isTest
    public static void testValidateUserInputNoCounty(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Educator';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'email';
		registerController.phone = '444-444-4444';
		registerController.city = 'milwaukee';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(false, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(1, messages.size());
		system.assertEquals('County is required.', messages[0].getSummary());
    }

    @isTest
    public static void testValidateUserInputNoState(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Educator';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'email';
		registerController.phone = '444-444-4444';
		registerController.city = 'milwaukee';
		registerController.county = 'milwaukee';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(false, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(1, messages.size());
		system.assertEquals('State is required.', messages[0].getSummary());
    }

	@isTest
    public static void testValidateUserInputNoZip(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Educator';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'email';
		registerController.phone = '444-444-4444';
		registerController.city = 'milwaukee';
		registerController.county = 'milwaukee';
		registerController.state = 'WI';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(false, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(1, messages.size());
		system.assertEquals('Zip is required.', messages[0].getSummary());
    }

    @isTest
    public static void testValidateUserInputEducatorNoSchoolDistrict(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Educator';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'email';
		registerController.phone = '444-444-4444';
		registerController.city = 'milwaukee';
		registerController.county = 'milwaukee';
		registerController.state = 'WI';
		registerController.zip = '53221';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(false, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(1, messages.size());
		system.assertEquals('School District is required.', messages[0].getSummary());
    }

    @isTest
    public static void testValidateUserInputEducatorNoSchool(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Educator';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'email';
		registerController.phone = '444-444-4444';
		registerController.city = 'milwaukee';
		registerController.county = 'milwaukee';
		registerController.state = 'WI';
		registerController.zip = '53221';
		registerController.schoolDistrict = '1211';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(false, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(1, messages.size());
		system.assertEquals('School is required.', messages[0].getSummary());
    }

    @isTest
    public static void testValidateUserInputEducatorNoJobTitle(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Educator';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'email';
		registerController.phone = '444-444-4444';
		registerController.city = 'milwaukee';
		registerController.county = 'milwaukee';
		registerController.state = 'WI';
		registerController.zip = '53221';
		registerController.schoolDistrict = '1211';
		registerController.school = 'milwaukee high';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(false, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(1, messages.size());
		system.assertEquals('Job Title is required.', messages[0].getSummary());
    }

    @isTest
    public static void testValidateUserInputEducatorValid(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Educator';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'email';
		registerController.phone = '444-444-4444';
		registerController.city = 'milwaukee';
		registerController.county = 'milwaukee';
		registerController.state = 'WI';
		registerController.zip = '53221';
		registerController.schoolDistrict = '1211';
		registerController.school = 'milwaukee high';
		registerController.jobTitle = 'Teacher';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(true, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(0, messages.size());
    }

    @isTest
    public static void testValidateUserInputEducatorMultipleNoSchoolDistrict(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'EducatorMultiple';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'email';
		registerController.phone = '444-444-4444';
		registerController.city = 'milwaukee';
		registerController.county = 'milwaukee';
		registerController.state = 'WI';
		registerController.zip = '53221';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(false, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(1, messages.size());
		system.assertEquals('School District is required.', messages[0].getSummary());
    }

    @isTest
    public static void testValidateUserInputEducatorMultipleNoJobTitle(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'EducatorMultiple';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'email';
		registerController.phone = '444-444-4444';
		registerController.city = 'milwaukee';
		registerController.county = 'milwaukee';
		registerController.state = 'WI';
		registerController.zip = '53221';
		registerController.schoolDistrict = '12321';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(false, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(1, messages.size());
		system.assertEquals('Job Title is required.', messages[0].getSummary());
    }

    @isTest
    public static void testValidateUserInputOrganizationalValid(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Organizational';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'email';
		registerController.phone = '444-444-4444';
		registerController.city = 'milwaukee';
		registerController.county = 'milwaukee';
		registerController.state = 'WI';
		registerController.zip = '53221';
		registerController.organizationValue = '1211';
		registerController.jobTitle = 'Teacher';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(true, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(0, messages.size());
    }

    @isTest
    public static void testValidateUserInputOrganizationalNoJob(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Organizational';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'email';
		registerController.phone = '444-444-4444';
		registerController.city = 'milwaukee';
		registerController.county = 'milwaukee';
		registerController.state = 'WI';
		registerController.zip = '53221';
		registerController.organizationValue = '1211';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(false, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(1, messages.size());
		system.assertEquals('Job Title is required.', messages[0].getSummary());
    }

    @isTest
    public static void testValidateUserInputOrganizationalNoOrganization(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Organizational';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'email';
		registerController.phone = '444-444-4444';
		registerController.city = 'milwaukee';
		registerController.county = 'milwaukee';
		registerController.state = 'WI';
		registerController.zip = '53221';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(false, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(1, messages.size());
		system.assertEquals('Organization is required.', messages[0].getSummary());
    }

    @isTest
    public static void testValidateUserInputIndividualValid(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Individual';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'email';
		registerController.phone = '444-444-4444';
		registerController.city = 'milwaukee';
		registerController.county = 'milwaukee';
		registerController.state = 'WI';
		registerController.zip = '53221';
		Boolean valid = registerController.validateUserInput();
		system.assertEquals(true, valid);

		ApexPages.Message[] messages = ApexPages.getMessages();
		system.assertEquals(0, messages.size());
    }

    /*
    @isTest
    public static void userExistsFalse(){
		Boolean exists = RegisterController.userExists('testuseremail@mail.com');
		system.assert(false, exists);
    }
    */

    @isTest
    public static void userExistsTrue(){
    	User u = [SELECT Email FROM User LIMIT 1];
		Boolean exists = RegisterController.userExists(u.Email);
		system.assert(true, exists);
    }

	@isTest
    public static void testgetProfileIdForUserEducator(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Educator';
		Id id = registerController.getProfileIdForUser();

		Id profileId = [
            SELECT Id, Name
            FROM Profile
            WHERE Name = 'Customer Community Plus K-8'
        ].Id;
        System.assertEquals(profileId, id);
	}

	@isTest
    public static void testgetProfileIdForUserEducatorMultiple(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'EducatorMultiple';
		Id id = registerController.getProfileIdForUser();

		Id profileId = [
            SELECT Id, Name
            FROM Profile
            WHERE Name = 'Customer Community Plus K-8 Multiple'
        ].Id;
        System.assertEquals(profileId, id);
	}

	@isTest
    public static void testgetProfileIdForUserOrganizational(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Organizational';
		Id id = registerController.getProfileIdForUser();

		Id profileId = [
            SELECT Id, Name
            FROM Profile
            WHERE Name = 'Customer Community Plus Individual'
        ].Id;
        System.assertEquals(profileId, id);
	}

	@isTest
    public static void testgetProfileIdForUserIndividual(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Individual';
		Id id = registerController.getProfileIdForUser();

		Id profileId = [
            SELECT Id, Name
            FROM Profile
            WHERE Name = 'Customer Community Plus Individual'
        ].Id;
        System.assertEquals(profileId, id);
	}

	@isTest
    public static void testCreateUser(){
    	Id profileId = [
            SELECT Id, Name
            FROM Profile
            WHERE Name = 'Customer Community Plus Individual'
        ].Id;

        Account a = new Account();
        a.OwnerId = [SELECT Id FROM User WHERE UserRoleId <> '' LIMIT 1].Id;
        a.Name = 'test';
        insert a;

        Contact c = new Contact();
        c.FirstName = 'Test';
        c.LastName = 'tester';
        c.AccountId = a.Id;
        insert c;

		RegisterController.createUser('first', 'last','test@test.com', 'username@test.com', 'tester', '44444444444', 'company', c.Id, profileId);
	}

	@isTest
    public static void testSetSchoolDistrictsEducator(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Educator';

		Account a = new Account();
		a.Name = 'testaccount';
		a.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' and DeveloperName = 'School_District'].Id;
		insert a;
		registerController.SetSchoolDistricts();

        System.assertEquals(1, registerController.schoolDistricts.size());
        System.assertEquals(true, registerController.showSchools);
	}

	@isTest
    public static void testSetSchoolDistrictsNotEducator(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Individual';

		Account a = new Account();
		a.Name = 'testaccount';
		a.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' and DeveloperName = 'School_District'].Id;
		insert a;
		registerController.SetSchoolDistricts();

        System.assertEquals(1, registerController.schoolDistricts.size());
        System.assertEquals(false, registerController.showSchools);
	}

	@isTest
    public static void testCreateContactEducator(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Educator';

		Account a = new Account();
		a.Name = 'testaccount';
		a.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' and DeveloperName = 'School_District'].Id;
		insert a;
		Contact c = registerController.CreateContact('first','lastName','email@test.com', '4444444444', 'city', 'county', 'state', 'zip', a.Id);

        System.assertNotEquals(null, c.Id);
	}

	@isTest
    public static void testCreateContactOrganizational(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Organizational';

		Account a = new Account();
		a.Name = 'testaccount';
		a.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' and DeveloperName = 'School_District'].Id;
		insert a;
		Contact c = registerController.CreateContact('first','lastName','email@test.com', '4444444444', 'city', 'county', 'state', 'zip', a.Id);

        System.assertNotEquals(null, c.Id);
	}

	@isTest
    public static void testCreateContactIndividual(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Individual';

		Account a = new Account();
		a.Name = 'testaccount';
		a.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' and DeveloperName = 'School_District'].Id;
		insert a;
		Contact c = registerController.CreateContact('first','lastName','email@test.com', '4444444444', 'city', 'county', 'state', 'zip', a.Id);

        System.assertNotEquals(null, c.Id);
	}

	@isTest
    public static void testSetSchoolsForDistrict(){
		RegisterController registerController = new RegisterController();

		Account a = new Account();
		a.Name = 'testschooldiscrict';
		a.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' and DeveloperName = 'School_District'].Id;
		insert a;

		Account a2 = new Account();
		a2.Name = 'school1';
		a2.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' and DeveloperName = 'School'].Id;
		a2.ParentId = a.Id;
		insert a2;

		Account a3 = new Account();
		a3.Name = 'school2';
		a3.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' and DeveloperName = 'School'].Id;
		a3.ParentId = a.Id;
		insert a3;
		registerController.schoolDistrict = a.Name;
		registerController.SetSchoolsForDistrict();
        System.assertEquals(3, registerController.schools.size());
	}

	@isTest
    public static void testGetAccountForContactEducator(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Educator';

		Account a = new Account();
		a.Name = 'testschooldiscrict';
		a.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' and DeveloperName = 'School_District'].Id;
		insert a;

		Account a2 = new Account();
		a2.Name = 'school1';
		a2.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' and DeveloperName = 'School'].Id;
		a2.ParentId = a.Id;
		insert a2;
		registerController.school = a2.Id;

		Account acc = registerController.GetAccountForContact();

        System.assertNotEquals(null, acc);
	}


	@isTest
    public static void testGetAccountForContactEducatorMultiple(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'EducatorMultiple';

		Account a = new Account();
		a.Name = 'testschooldiscrict';
		a.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' and DeveloperName = 'School_District'].Id;
		insert a;

		registerController.schoolDistrict = 'testschooldiscrict';

		Account acc = registerController.GetAccountForContact();

        System.assertNotEquals(null, acc);
	}

	@isTest
    public static void testGetAccountForContactOrganizationExistingOrg(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Organizational';

		Account a = new Account();
		a.Name = 'testorg';
		a.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' and DeveloperName = 'Organization'].Id;
		insert a;

		registerController.organizationValue = 'testorg';

		Account acc = registerController.GetAccountForContact();

        System.assertNotEquals(null, acc);
	}

	@isTest
    public static void testGetAccountForContactOrganizationNewOrg(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Organizational';

		registerController.organizationValue = 'testorg';

		Account acc = registerController.GetAccountForContact();

        System.assertNotEquals(null, acc);
	}

	@isTest
    public static void testGetAccountForContactIndividual(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Individual';

		registerController.firstName = 'first';
		registerController.lastName = 'last';

		Account acc = registerController.GetAccountForContact();

        System.assertNotEquals(null, acc);
	}

	@isTest
    public static void testRegisterUserIndividualValid(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Individual';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'test@email.com';
		registerController.phone = '444-444-4444';
		registerController.city = 'milwaukee';
		registerController.county = 'milwaukee';
		registerController.state = 'WI';
		registerController.zip = '53221';
		PageReference pr = registerController.registerUser();
		system.assertNotEquals(null, pr);
    }

    @isTest
    public static void testRegisterUserIndividualInvalidEmail(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Individual';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'test';
		registerController.phone = '444-444-4444';
		registerController.city = 'milwaukee';
		registerController.county = 'milwaukee';
		registerController.state = 'WI';
		registerController.zip = '53221';
		PageReference pr = registerController.registerUser();
		system.assertNotEquals(null, pr);
    }

    @isTest
    public static void testRegisterUserOrganizationalValid(){
		RegisterController registerController = new RegisterController();
		registerController.courseType = 'Organizational';
		registerController.firstName = 'First';
		registerController.lastName = 'Last';
		registerController.email = 'test@email.com2';
		registerController.phone = '444-444-4444';
		registerController.city = 'milwaukee';
		registerController.county = 'milwaukee';
		registerController.state = 'WI';
		registerController.zip = '53221';
		registerController.organizationValue = 'test';
		registerController.jobTitle = 'job';
		PageReference pr = registerController.registerUser();
		system.assertNotEquals(null, pr);
    }

}