public class RegistrationTriggerHelper {
    public static void addEnrollments (List<Registration__c> lReg) {
        List<enlighten__Enrollment__c> lEnrollToAdd = new List<enlighten__Enrollment__c>();
    	List<enlighten__Course_User_Group__c> lUGtoAdd = new List<enlighten__Course_User_Group__c>();
    	// get roles
    	ID sRoleID = [SELECT ID FROM enlighten__Role__c WHERE Name='Student' LIMIT 1].ID;
    	ID teachRoleID = [SELECT ID FROM enlighten__Role__c WHERE Name='Non-editing teacher' LIMIT 1].ID;
    	for (Registration__c reg : lReg) {
      	  	// get course
      	  	Enlighten__course__c course = [SELECT ID,RecordType.Name FROM Enlighten__Course__c WHERE ID=:reg.Course__c LIMIT 1];
        	// if course<>k-8, create enrollment for contact
        	if (course.RecordType.Name=='Individual') {
            	Enlighten__Enrollment__c newEnroll = new Enlighten__Enrollment__c(enlighten__Contact__c=reg.Contact__c, enlighten__Course__c=reg.Course__c, enlighten__Role__c=sRoleID, Registration__c=reg.ID);
            	lEnrollToAdd.add(newEnroll);
            	System.debug('Individual enrollment');
        	}
        	// if course-k-8, get classroom, get students, for each student create an enrollment
        	else if (course.RecordType.Name=='K-8') {
            	//get classroom
            	Classroom__c cRoom = [SELECT ID, Name FROM Classroom__c WHERE ID=:reg.Classroom__c LIMIT 1];
            	System.debug('Classroom: '+cRoom.ID);
            	//get students
            	List<Contact> lStudents = [SELECT ID FROM Contact WHERE RecordType.Name='Student' AND Classroom__c=: cRoom.ID];
            	System.debug('Num students: '+lStudents.size());
            	// get teacher info
            	Contact teach = [SELECT ID, FirstName, LastName FROM Contact WHERE ID=:reg.Contact__c LIMIT 1];
            	// create course user group
            	enlighten__Course_User_Group__c usergroup = new enlighten__Course_User_Group__c(Name=cRoom.Name+teach.firstname+teach.LastName+math.round(math.random()*999), enlighten__Course__c=reg.Course__c, Registration__c=reg.ID);
            	lUGtoAdd.add(usergroup);
<<<<<<< HEAD
                // check if teacher is already enrolled in course and if not, create enrollment for teacher
                if ([SELECT ID FROM enlighten__Enrollment__c WHERE enlighten__Contact__c=:teach.ID AND enlighten__Course__c=:reg.Course__c].size() == 0) {
            		enlighten__Enrollment__c newEnroll = new Enlighten__Enrollment__c(enlighten__Contact__c=reg.Contact__c, enlighten__Course__c=reg.Course__c, enlighten__Role__c=teachRoleID, Registration__c=reg.ID);
            		lEnrollToAdd.add(newEnroll);
                }
=======
            	// create enrollment for teacher
            	enlighten__Enrollment__c newEnroll = new Enlighten__Enrollment__c(enlighten__Contact__c=reg.Contact__c, enlighten__Course__c=reg.Course__c, enlighten__Role__c=teachRoleID, Registration__c=reg.ID);
            	lEnrollToAdd.add(newEnroll);
            	
>>>>>>> c52b006bf2e489d25cd63182dfd52eebbc36f7f1
            	//create an enrollment for each student
            	if (!lStudents.isEmpty()) {
                	for (Contact c : lStudents) {
                  	  enlighten__Enrollment__c newStEnroll = new enlighten__Enrollment__c(enlighten__Contact__c=c.Id, enlighten__Course__c=reg.Course__c, enlighten__Role__c=sRoleID, Registration__c=reg.ID, enlighten__ComboID__c='TempId-'+c.ID+reg.Course__c);
                  	  lEnrollToAdd.add(newStEnroll);
                	}
            	}
        	}
    	}
    	insert(lEnrollToAdd);
    	insert(lUGtoAdd);
    }
    public static void addUGA (Map<Id, Registration__c> newRegs, Map<Id, Registration__c> oldRegs) {
        List<Registration__c> lRegadd = new List<Registration__c> ();
        List<enlighten__Course_User_Group_Assignment__c> lUGA = new List<enlighten__Course_User_Group_Assignment__c> ();
        for (Registration__c reg : newRegs.values()) {
            // if createUGA=true, get enrollments           
            if (reg.create_UGA__c==true && oldRegs.get(reg.ID).create_UGA__c==false) {
            	// get User Group
            	ID UGid = [SELECT ID FROM enlighten__Course_User_Group__c WHERE Registration__c=:reg.ID LIMIT 1].ID;
                for (enlighten__Enrollment__c enroll : [SELECT ID, enlighten__Contact__c FROM enlighten__Enrollment__c WHERE Registration__c=:Reg.ID]) {
                    enlighten__Course_User_Group_Assignment__c newUGA = new enlighten__Course_User_Group_Assignment__c(enlighten__Group__c=UGid, enlighten__Enrollment__c=enroll.ID, enlighten__Push__c=true, enlighten__ComboID__c='TempId-'+enroll.ID+UGid);
                	lUGA.add(newUGA);
                }
            }
            // for each enrollment, create a UGA
        }
        insert lUGA;
    }
    public static void duplicateCheck (List<Registration__c> lReg) {
        for (Registration__c reg : lReg) {
            // if classroom is blank, check for existing reg with matching course and contact
            if (reg.Classroom__c==null) {
                List<Registration__c> matches = [SELECT ID FROM Registration__c WHERE Course__c=:reg.Course__c AND Contact__c=:reg.Contact__c];
                if (matches.size() > 0)
                    reg.addError('You already have a registration for this course');
            }
        	// if classroom isn't blank, check for existing reg with matching course and classroom
            else if (reg.Classroom__c <> null) {
        		List<Registration__c> match = [SELECT ID FROM Registration__c WHERE Course__c=:reg.Course__c AND Classroom__c=:reg.Classroom__c];
            	if (match.size() > 0)
                    reg.addError('You already have a registration for this classroom for this course');
            }
        }
    }
}